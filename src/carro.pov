#version 3.7;

#include "roda.pov"
#include "grade.pov"
#include "monobloco.pov"
#include "detalhes.pov"
#include "reator.pov"
#include "cenario.pov"

#include "colors.inc"    
#include "textures.inc"  
#include "shapes.inc"

// Lado direito
#declare camera_1 =
camera {
  location <700,100,0>
  look_at <0, 0, 0>
}

// Lado Esquerdo
#declare camera_2 =
camera {
  location <-700,100,0>
  look_at <0, 0, 0>
}

// Direita - Frente
#declare camera_3 =
camera {
  location <400,100,-300>
  look_at <0, 0, 0>
}

// Esquerda - Frente
#declare camera_4 =
camera {
  location <-300,100,-300>
  look_at <0, 0, 0>
}

// Frente total
#declare camera_5 =
camera {
  location <0,100,-600>
  look_at <0, 0, 0>
}

// Traseira total
#declare camera_6 =
camera {
  location <0,100,500>
  look_at <0, 0, 0>
}

//Cima
#declare camera_7 =
camera {
  location <0,1000,0>
  look_at <0, 0, 0>
}

// Traseira - Alto
#declare camera_8 =
camera {
  location <0,400,200>
  look_at <0, 0, 0>
}

// Traseira - Lado
#declare camera_9 =
camera {
  location <600,100,600>
  look_at <0, 0, 0>
}

camera{camera_8}

light_source { 
  <600, 100, 0>
  color White 
}

light_source { 
  <-1000, 200, 0>
  color White 
}

light_source { 
  <0, 200, 1000>
  color White 
}

#declare carro = union{
  difference{
    union{
      object{monobloco}
      object{saia_frente1 translate <162, 11, 65>}
      object{saia_traseira1 translate <161, 15, 340>}
      object{saia_frente2 translate <0, -11, -65> rotate <180,0,0>}
      object{saia_traseira2 translate <0, -15, -340> rotate <180,0,0>}
    }
    union{
      object{cortes}
      object{porta}
      object{vidro_lateral}
      object{vidro_traseiro}
    }
    texture{
      Chrome_Metal
    }
  }

  object{vidro_frente}

  object{grade_parachoque}
  object{grade_meio}

  object{caixa_frente translate <165,0,0>}
  object{caixa_frente translate <-5,0,0>}

  object{volante}
  object{bancos}
  object{frizzos}
  object{retrovisor}
  object{spoiler_lateral}
  object{spoiler_traseira}
  object{spoiler_frente}
  object{rodas}
  object{rodas rotate <0,0,180> translate <165,30,0>}
  object{parachoque}
  object{parachoque_traseiro}
  object{bloco_traseiro}
  object{saida_ar scale 0.3 translate <-5,45,380>}
  object{saida_ar scale 0.3 translate <13,45,380>}
  object{saida_ar scale 0.3 translate <73,45,380>}
  object{saida_ar scale 0.3 translate <91,45,380>}

  object{farol_traseiro2}
  object{farol_traseiro1}
  object{placa}

  union{
    object{radiador}
    object{caixas_reator}
    object{ciclindros_reator}
    translate <83,90,370>
  }


  difference{
    union{
      object{frente scale 5.9 rotate <0,90,0> translate <5,45,0>}
      object{grade_traseira}
    }
    object{lateral}
    pigment
    {
      color <0.3,0.3,0.3>
    }
  }
  translate<-82.5,-57,-210.5>
}

object{carro}