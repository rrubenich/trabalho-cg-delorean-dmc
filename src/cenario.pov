#version 3.7;

#include "colors.inc"    
#include "textures.inc"  
#include "shapes.inc"
#include "stars.inc"
/*

sphere{ 
    <0,0,0>, 0.4 
    scale <1,1,1> 
    rotate <0,0,0> 
    translate<0.6,0.7,0.2>
    texture{ Polished_Chrome }
}

plane{ <0,1,0>, -75
  pigment{
    color White
  }
}

*/

fog{
    fog_type   2
    distance   1000
    color      rgb<1,1,1>*0.5
    fog_offset 0.1
    fog_alt    5
    turbulence 0.8
}
 

plane { 
    <0,1,0>, -75 
    texture{
        pigment{color White *0.7}
         normal {bumps 0.75 scale 0.015}
    }
} 


sphere{ 
    <0,0,0>, 0.4 
    scale <1,1,1> 
    rotate <0,0,0> 
    translate<0.6,0.7,0.2>
    texture{ Polished_Chrome }
}