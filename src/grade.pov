#version 3.7;

#include "colors.inc"    
#include "textures.inc"  
#include "shapes.inc"

camera {      
  angle 90
  location <20, 0, 0>
  look_at <0, 0, 13>
}

light_source { 
  <30, 5, 0> 
  color White 
}


// Logo
#declare logo = union{
  box{
    <0,0.6,11.5>
    <1.20,1.25,12.4>
    texture{
      Chrome_Metal
    }
  }
  box{
    <0,0.85,11.5>
    <1.21,0.95,12.1>
    pigment{
      color Black
    }
  }
  box{
    <0,0.6,12.5>
    <1.20,1.25,13.4>
    texture{
      Chrome_Metal
    }
  }
  box{
    <0,0.5,12.73>
    <1.21,1,12.80>
    pigment{
      color Black
    }
  }
  box{
    <0,0.5,13>
    <1.21,1,13.1>
    pigment{
      color Black
    }
  }

  box{
    <0,0.6,13.5>
    <1.20,1.25,14.4>
    texture{
      Chrome_Metal
    }
  }
  box{
    <0,0.85,13.7>
    <1.21,0.95,14.4>
    pigment{
      color Blue
    }
  }
}

//Raios e espaço para logo
#declare centro_grade = merge{
  box{
    <0,0.5,11.5>
    <1.15,1.35,14.5>
    pigment{
      color Black
    }
  }

  union{
    box{
      <0.9,0,6.1>
      <1.1,0.07,19.9>
    }

    box{
      <0.9,0.2,6.1>
      <1.1,0.27,19.9>
    }

    box{
      <0.9,0.4,6.1>
      <1.1,0.47,19.9>
    }

    box{
      <0.9,0.6,6.1>
      <1.1,0.67,19.9>
    }

    box{
      <0.9,0.8,6.1>
      <1.1,0.87,19.9>
    }

    box{
      <0.9,1,6.1>
      <1.1,1.07,19.9>
    }

    box{
      <0.9,1.2,6.1>
      <1.1,1.27,19.9>
    }

    box{
      <0.9,1.4,6.1>
      <1.1,1.47,19.9>
    }

    box{
      <0.9,1.6,6.1>
      <1.1,1.67,19.9>
    }

    box{
      <0.9,1.8,6.1>
      <1.1,1.87,19.9>
    }

    texture{
      Chrome_Metal
    }
  }
}


#declare farol = union{
  object {
    Round_Box(<0,0,0>,<1,2,2.9>, 0.1, 0)
    pigment{
      color White
    }
  }

  object {
    Round_Box(<0,0,3.1>,<1,2,6>, 0.1, 0)
    pigment{
      color White
    }
  }
}

#declare grade = object {
  Round_Box(<-1,-0.1,-0.1>,
  <0.9,2.1,26.1>,
  0.1,
  0)

  pigment{
    color Black
  }
}

#declare corte_grade = object {
  Round_Box(<-1,-0.1,-0.1>,
  <2,2.1,26.1>,
  0.1,
  0)

  pigment{
    color <0.3,0.3,0.3>
  }
}

#declare externo_grade = 
union{
  object {
    Round_Box(<-1,-1,-0.7>,<2,2.5,27>, 0.2, 0)
    rotate <0,0,20>
  }
  object {
    Round_Box(<-1,0,-0.5>,<1,3,27>, 0.2, 0)
  }
  pigment{
    color <0.3,0.3,0.3>
  }
}

#declare interno_frente = union{
  object{farol}
  object{farol translate <0,0,20>}
  object{centro_grade}
  object{grade}
  object{logo}
}

#declare frente = union{
  object{interno_frente}
  difference{
    object{externo_grade}
    object{corte_grade}
  }
}
