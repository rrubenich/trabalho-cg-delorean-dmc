#version 3.7;

#include "colors.inc"    
#include "textures.inc"  
#include "stones1.inc"

#declare pneu = union{
  cylinder
  {
    <0, 0, 0>, 
    <4, 0, 0>, 
    7
  }

  torus{
    5.5,
    1.5
    rotate <0,0,90>
    translate <4,0,0>
  }

  torus{
    5.5,
    1.5
    rotate <0,0,90>
    translate <0,0,0>
  }


  pigment{
    color rgb<1,1,1>*0.15
  }
}

#declare aro = difference{
  difference{
    cylinder
    {
      <4, 0, 0>, 
      <5, 0, 0>, 
      5
    }

    torus{
      3,
      1.5
      rotate <0,0,90>
      translate <4,0,0>
    }

    texture{
      Chrome_Metal
    }
  }
  cylinder
  {
    <4, 0, 0>, 
    <7, 0, 0>, 
    0.6
  }
}

#declare centro = cylinder{
  <4, 0, 0>, 
  <5, 0, 0>, 
  0.6
  pigment{
    color Black
  }
}

// Parafusos
#declare parafusos = union{
  cylinder
  {
    <4, -1.2, 0>, 
    <5.1, -1.2, 0>, 
    0.3
  }
  cylinder
  {
    <4, 1.2, 0>, 
    <5.1, 1.2, 0>, 
    0.3
  }
  cylinder
  {
    <4, 0, 1.2>, 
    <5.1, 0, 1.2>, 
    0.3
  }
  cylinder
  {
    <4, 0, -1.2>, 
    <5.1, 0, -1.2>, 
    0.3
  }  
  texture{
    Brushed_Aluminum
  }
}

// Raio
#declare raio = box{
  <3,-5,-0.1>, <4.9, 5, 0.1>
}

#declare raios = union{
  object{raio rotate <0,0,0>}
  object{raio rotate <10,0,0>}
  object{raio rotate <20,0,0>}
  object{raio rotate <30,0,0>}
  object{raio rotate <40,0,0>}
  object{raio rotate <50,0,0>}
  object{raio rotate <60,0,0>}
  object{raio rotate <70,0,0>}
  object{raio rotate <80,0,0>}
  object{raio rotate <90,0,0>}
  object{raio rotate <100,0,0>}
  object{raio rotate <110,0,0>}
  object{raio rotate <120,0,0>}
  object{raio rotate <130,0,0>}
  object{raio rotate <140,0,0>}
  object{raio rotate <150,0,0>}
  object{raio rotate <160,0,0>}
  object{raio rotate <170,0,0>}
  
  texture{
    Chrome_Metal
  }
}

#declare roda = union{
  object{pneu}
  object{aro}
  object{centro}
  object{parafusos}
  object{raios}
}
