#version 3.7;

#include "roda.pov"
#include "colors.inc"  
#include "textures.inc"
#include "shapes.inc"

#declare bloco_saida = difference{
  box{
    <101,0,0>
    <165,160,421>
    rotate <-25,0,0>
  }

  union{
    box{
      <100,0,200>
      <166,180,421>
      rotate <-15,0,0>
      translate <0,65,30>
    }
    box{
      <100,-10,170>
      <166,180,421>
      rotate <5,0,0>
      translate <0,100,30>
    }
  }

}

#declare saida_ar = 
union{
  box{
    <101,215,100>
    <165,210,140>
    translate <0,0,30>
  }  
  
  box{
    <101,165,100>
    <165,160,140>
    translate <0,0,30>
  }  
  
  box{
    <101,115,100>
    <165,110,140>
    translate <0,0,30>
  }  

  difference{
    object{bloco_saida}
    object{bloco_saida scale 0.9 translate <15,25,50> }

  }

  pigment{
    color <0.05,0.05,0.05>
  }

}

#declare radiador_aro =   box{
  <0,10,0>
  <1,11,17>
}


#declare radiador_bloco =   box{
  <0,10,0>
  <1,11,17>
}

#declare radiador = union{
  
  cylinder{
    <0,11,0>,
    <0,-10,0>,
    6
    pigment{
      color Yellow
    }
  }
  
  cylinder{
    <0,10,0>,
    <0,5,0>,
    13
  }

  cylinder{
    <0,5,0>,
    <0,-5,0>,
    11
    pigment{
      color Yellow
    }
  }

  cylinder{
    <0,10.2,0>,
    <0,5,0>,
    11
  }

  cylinder{
    <0,1,0>,
    <0,-10,0>,
    20
  }

  object{radiador_aro rotate <30,10,0>}
  object{radiador_aro rotate <30,20,0>}
  object{radiador_aro rotate <30,30,0>}
  object{radiador_aro rotate <30,40,0>}
  object{radiador_aro rotate <30,50,0>}
  object{radiador_aro rotate <30,60,0>}
  object{radiador_aro rotate <30,70,0>}
  object{radiador_aro rotate <30,80,0>}
  object{radiador_aro rotate <30,90,0>}
  object{radiador_aro rotate <30,100,0>}
  object{radiador_aro rotate <30,110,0>}
  object{radiador_aro rotate <30,120,0>}
  object{radiador_aro rotate <30,130,0>}
  object{radiador_aro rotate <30,140,0>}
  object{radiador_aro rotate <30,150,0>}
  object{radiador_aro rotate <30,160,0>}
  object{radiador_aro rotate <30,170,0>}
  object{radiador_aro rotate <30,180,0>}
  object{radiador_aro rotate <30,190,0>}
  object{radiador_aro rotate <30,200,0>}
  object{radiador_aro rotate <30,210,0>}
  object{radiador_aro rotate <30,220,0>}
  object{radiador_aro rotate <30,230,0>}
  object{radiador_aro rotate <30,240,0>}
  object{radiador_aro rotate <30,250,0>}
  object{radiador_aro rotate <30,260,0>}
  object{radiador_aro rotate <30,270,0>}
  object{radiador_aro rotate <30,280,0>}
  object{radiador_aro rotate <30,290,0>}
  object{radiador_aro rotate <30,300,0>}
  object{radiador_aro rotate <30,310,0>}
  object{radiador_aro rotate <30,320,0>}
  object{radiador_aro rotate <30,330,0>}
  object{radiador_aro rotate <30,340,0>}
  object{radiador_aro rotate <30,350,0>}
  object{radiador_aro rotate <30,360,0>}

  pigment{
    color <0.1,0.1,0.1>
  }
}


#declare caixas_reator = union{
  box{
    <-17,-40,-90>
    <17,-5,-30>
    rotate <20,0,0>
  }

  box{
    <-5,-10,40>
    <5,-8,45>
  }

  box{
    <10,20,-65>
    <-10,10,-55>
    pigment{
      color <0.05,0.05,0.05>
    }
  }

  union{
    box{
      <-20,-20,-55>
      <-30,10,-45>
    }

    box{
      <20,-20,-55>
      <30,10,-45>
    }

    pigment{
      color <0.05,0.05,0.05>
    }
  }

  union{
    box{
      <-20,10,-55>
      <-30,20,-45>
    }

    box{
      <20,10,-55>
      <30,20,-45>
    }

    pigment{
      color Blue
    }
  }

  box{
    <-3,10,-93>
    <3,35,-97>
  }
  
  box{
    <-10,33,-90>
    <10,37,-105>
  }

  texture{
    Chrome_Metal
  }
}


#declare ciclindros_reator = union{

  cylinder{
    <0,15,-90>,
    <0,15,-30>,
    3
    open
    rotate <5,0,0>
  }

  cylinder{
    <-25,15,-50>,
    <25,15,-50>,
    2.5
    pigment{
      color <0.1,0.1,0.1>
    }
  }  

  cylinder{
    <0,-10,30>,
    <25,-10,30>,
    2.5
    rotate <0,0,30>
  }

  cylinder{
    <0,-10,30>,
    <-25,-10,30>,
    2.5
    rotate <0,0,-30>
  }

  cylinder{
    <-50,5,-90>,
    <-50,5,10>,
    1
    open
    rotate <10,0,0>
  }

  cylinder{
    <50,5,-90>,
    <50,5,10>,
    1
    open
    rotate <10,0,0>
  }

  cylinder{
    <-60,3,-60>,
    <-60,3,-100>,
    1
  }

  union{
    cylinder{
      <-60,3,-60>,
      <-60,3,-10>,
      2
    }

    cylinder{
      <-60,-10,-60>,
      <-60,-10,-20>,
      4
    }

    pigment{
      color <0,0.1,0>
    }
  }
  union{
    cylinder{
      <60,7,-30>,
      <60,-10,-30>,
      4
    }

    cylinder{
      <60,7,-45>,
      <60,-10,-45>,
      4
    }

    cylinder{
      <60,7,-60>,
      <60,-10,-60>,
      4
    }
    pigment{
      color Red
    }
  }

  union{
    cylinder{
      <60,-20,-30>,
      <0,-20,-30>,
      1
    }

    cylinder{
      <60,-20,-45>,
      <0,-20,-45>,
      1
    }

    cylinder{
      <60,-20,-60>,
      <0,-20,-60>,
      1
    }

    rotate <0,0,20>
  }

  pigment{
    color <0.2,0.2,0.2>
  }
}