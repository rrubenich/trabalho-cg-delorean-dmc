#version 3.7;

#include "roda.pov"

#include "colors.inc"    
#include "textures.inc"  
#include "shapes.inc"

camera {
  location <900,100,0>
  look_at <0, 0, 0>
}

#declare frizzos = box{
  <-1,45,0>
  <165.1,48,421>
  pigment{
    color <0.05,0.05,0.05>
  }
}

#declare vidro_frente = difference{    
  box{
    <0,77,101>
    <165,112,180>
  }

  union{
    box{
      <-1,28,125>
      <166,114,210>
      rotate <-25,0,0>
    }
    box{
      <-1,25,125>
      <166,-60,250>
      rotate <-25,0,0>
    }
    object{corte_laterais_interno}
  }

  texture{ pigment{color <0.02,0.02,0.02> filter 0.2 }}
}

#declare rodas = union{
  object{roda scale 4.6 translate <143,14,65>}
  object{roda scale 5 translate <143,14,340>}
}

#declare spoiler_lateral = union{
  box{
    <-1,7,100>
    <168,22,298>
  }
  pigment{
    color <0.1,0.1,0.1>
  }
}

#declare spoiler_frente = union{
  box{
    <0,10,-10>
    <166,24,25>
    rotate <10,0,0>
  }
  pigment{
    color <0.1,0.1,0.1>
  }
}

#declare spoiler_traseira = union{
  box{
    <0,-95,370>
    <166,-100,412>
    rotate <-17,0,0>
  }
  pigment{
    color <0.1,0.1,0.1>
  }
}

#declare parachoque = difference{
  box{
    <1,24,-13>
    <165,44,0>

    pigment{
      color <0.05,0.05,0.05>
    }
  }
  box{
    <-1,10,-20>
    <167,24,25>
    rotate <10,0,0>
  }
}

#declare grade_parachoque = union{
  object {
    Round_Box(<-1,25,-15>,<167,40,0>, 3, 0)

    texture{
      Brushed_Aluminum
    }
  }
  object {
    Round_Box(<-1.1,28,-15.1>,<167.1,37,0>, 1, 0)
    texture{
      Copper_Metal
    }
  }

}


#declare bloco_traseiro = difference{
  box{
    <0,55,285>
    <165,75,422>
  }

  union{
    box{
      <0,-10,-20>
      <25,220,450>
      rotate <0,0,-16>
      translate <-34,0,0>
    }
    box{
      <0,-10,-20>
      <25,220,450>
      rotate <0,0,16>
      translate <173,0,0>
    }
  }
  pigment{
    color <0.05,0.05,0.05>
  }
}

#declare farol_traseiro1 = difference{
  union{
    
    box{
      <0,57,285>
      <19,73,422.2>
      pigment{
        color Orange *0.2
      }
    }
    box{
      <20,57,285>
      <49,73,422.2>
      pigment{
        color Red *0.2
      }
    }
    box{
      <50,57,285>
      <55,73,422.2>
      pigment{
        color White *0.5
      }
    }
  }

  box{
    <0,-10,-20>
    <25,220,450>
    rotate <0,0,-16>
    translate <-31,0,0>
  }

}


#declare farol_traseiro2 = difference{
  union{
    
    box{
      <145,57,285>
      <155,73,422.2>
      pigment{
        color Orange *0.2
      }
    }
    box{
      <116,57,285>
      <144,73,422.2>
      pigment{
        color Red *0.2
      }
    }
    box{
      <110,57,285>
      <115,73,422.2>
      pigment{
        color White *0.5
      }
    }
  }

  box{
    <0,-10,-20>
    <25,220,450>
    rotate <0,0,16>
    translate <170,0,0>
  }
  pigment{
    color Yellow
  }
}

#declare placa = 
union{
  box{
    <65,57,285>
    <100,73,422.2>

    pigment{
      color White
    }
  }
}

#declare parachoque_traseiro = 
  box{
    <0,25,405>
    <166,50,422>
    pigment{
      color <0.05,0.05,0.05>
    }
}

#declare grade_traseira = difference{
  box{
    <0,50,405>
    <165,100,420>
  }
  box{
    <-1,185,250>
    <166,220,420>
    rotate <15,0,0>
  }
  pigment{
    color <0.3,0.3,0.3>
  }
}


#declare porta = union{
  box{
    <-10,-30,125>
    <10,114,126>
    rotate <-20,0,0>
  }

  box{
    <146,-30,125>
    <176,114,126>
    rotate <-20,0,0>
  }

  box{
    <-10,150,112>
    <180,260,113>
    rotate <50,0,0>
  }

  box{
    <-10,80,270>
    <40,114,271>
  }

  box{
    <140,80,270>
    <180,114,271>
  }
  
  pigment{
    color Black
  }
}


#declare caixa_frente = union{
  box{
    <5,27,-5>
    <0,40,25>
    pigment{
      color <0.05,0.05,0.05>
    }
  }

  box{
    <5.2,29,0>
    <-0.2,38,7>
    texture{
      Brushed_Aluminum
    }
  }

  box{
    <5.2,32,9>
    <-0.2,38,23>
    texture{
      Brushed_Aluminum
    }
  }
}

#declare grade_meio = 

difference{
  union{  
    object {
      Round_Box(<150,75,272>,<166,70,395>, 5, 0)
    }
    object {
      Round_Box(<-15,75,272>,<10,70,395>, 5, 0)
    }

    box{
      <-0.5,-90,390>
      <166.5,-30,400>
      rotate <-15,0,0>
    }

    box{
      <0,117,272>
      <166,75,282>
    }

    box{
      <0,113,282>
      <166,75,282.2>
      pigment{
        color <0.05,0.05,0.05>
      }
    }
  }
  union{
    box{
      <0,-10,-20>
      <25,220,422>
      rotate <0,0,-16>
      translate <-42,0,0>
    }
    box{
      <0,-10,-20>
      <25,220,422>
      rotate <0,0,16>
      translate <179,0,0>
    }
  }
  texture{
      Copper_Metal
  }
}

#declare retrovisor = union{
  box{
    <154,77,125>
    <165,70,135>
    rotate <-5,0,0>
  }
  box{
    <11,77,125>
    <0,70,135>
      rotate <-5,0,0>
  }
  pigment{
    color <0.2,0.2,0.2>
  }
}

#declare bancos = difference{
  union{
    box{
      <20,28,250>
      <147,85,270>
    }
    box{
      <40,85,250>
      <60,100,270>
    }
    box{
      <111,85,250>
      <131,100,270>
    }
  }
  box{
    <80,28,250>
    <100,85,270>
  }
  
  pigment{
    color <0.2,0.2,0.2>
  }
}


#declare volante = cylinder{
  <130,80,140>,
  <130,80,143>,
  12
  pigment{
    color <0.2,0.2,0.2>
  }

}