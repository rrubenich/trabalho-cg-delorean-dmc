#version 3.7;

#include "colors.inc"    
#include "textures.inc"  
#include "shapes.inc"


#declare corte_laterais_interno = union{
  box{
    <0,-10,-1>
    <25,220,422>
    rotate <0,0,-16>
    translate <-34,0,0>
  }
  box{
    <0,-10,-1>
    <25,220,422>
    rotate <0,0,16>
    translate <173,0,0>
  }
}
  
#declare interno = 
difference{
  union{
    box{
      <0,77,0>
      <165,112,265>
    }
    box{
      <0,77,285>
      <165,112,422>
    }
  }

  object{corte_laterais_interno}
  
  pigment{
    color <0.05,0.05,0.05>
  }
}



#declare cortes =
union{
  // Baixo
  box{
    <-10,10,0>
    <200,-50,421>
  }
  // Capuz
  box
  {
    <-1,61.8,0>
    <166,114,115>
    rotate <-9,0,0>
  }

  // Interno
  box{
    <20,28,140>
    <126,95,270>
    pigment{
      color <0.05,0.05,0.05>
    }
  }

  // Para-brisa
  box{
    <-1,28,125>
    <166,114,210>
    rotate <-25,0,0>
  }
  // Para-brisa trás
  box{
    <-1,185,250>
    <166,220,421>
    rotate <15,0,0>
  }
  // Eixo 1
  cylinder{
    <-20,10,65>
    <180,10,65>
    38
    pigment{
      color <0.05,0.05,0.05>
    }
  }
  // Eixo 2
  cylinder{
    <-20,14,340>
    <180,14,340>
    41
    pigment{
      color <0.05,0.05,0.05>
    }
  }
  // Para-lama frente
  box{
    <-1,5,-10>
    <180,22,40>
    rotate <10,0,0>
  }
  // Para-lama atrás
  box{
    <-1,-95,353>
    <180,-135,421>
    rotate <-17,0,0>
  }

  // Parte baixo
  object{interno}
}


#declare vidro_lateral = 
difference{

  union{
    object {
      Round_Box(<-0.9,78,123>,<165.1,113,255>, 5, 0)
    }
  }

  box{
    <-1,26,125>
    <166,114,220>
    rotate <-25,0,0>
  }
}


#declare vidro_traseiro = 
difference{
  union{
    object {
      Round_Box(<-0.9,83,285>,<165.1,113,420>, 5, 0)
    }
  }
  box{
    <-1,182,200>
    <166,220,421>
    rotate <15,0,0>
  }
}


#declare lateral = union{
  box{
    <0,-10,-20>
    <25,220,422>
    rotate <0,0,-16>
    translate <-39,0,0>
  }
  box{
    <0,-10,-20>
    <25,220,422>
    rotate <0,0,16>
    translate <178,0,0>
  }
}


#declare saia_frente1 = cone{
  <0, 0.3, 0>, 42 
  <5, 0, 0>, 38
}

#declare saia_traseira1 = cone{
  <0, 0.3, 0>, 45 
  <5, 0, 0>, 42
}

#declare saia_frente2 = cone{
  <5, 0, 0>, 42 
  <0, 0.3, 0>, 38
}

#declare saia_traseira2 = cone{
  <5, 0, 0>, 45 
  <0, 0.3, 0>, 42
}


#declare monobloco = 
difference{
  union{
    box{
      <0,10,0>
      <165,114,405>
    }
  }
  object{lateral}
}
